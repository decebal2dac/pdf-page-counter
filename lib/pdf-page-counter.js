let PDFJS = require('pdfjs-dist')

const DEFAULT_OPTIONS = {
    max: 0,
}

async function PDF(dataBuffer, options) {
    let ret = {
        numpages: 0,
        info: null,
        metadata: null,
        text: "",
        version: null
    };

    if (typeof options == 'undefined') options = DEFAULT_OPTIONS;
    if (typeof options.max != 'number') options.max = DEFAULT_OPTIONS.max;

    ret.version = PDFJS.version;

    // Disable workers to avoid yet another cross-origin issue (workers need
    // the URL of the script to be loaded, and dynamically loading a cross-origin
    // script does not work).
    PDFJS.disableWorker = true;
    let doc = await PDFJS.getDocument(dataBuffer).promise;
    ret.numpages = doc.numPages;

    doc.destroy();

    return ret;
}

module.exports = PDF;
