const assert = require('assert');
const PDF = require('../');
const fs = require('fs');

// to test another valid pdf file just change this 5 constants.
const PDF_FILE = './test/data/04-valid.pdf';
const VERSION = 'default';
const PDF_PAGE_COUNT = 5;

describe(`File:${PDF_FILE} PDF.js Version:${VERSION}`, function() {
    this.timeout(20000);
    let dataBuffer = fs.readFileSync(PDF_FILE);
    it('should pass parse', function() {
        let options = {
            version: VERSION
        };

        return PDF(dataBuffer, options).then(function(data) {
            assert.equal(data.numpages, PDF_PAGE_COUNT);
        });
    });

    it('should pass parse with option max:-1', function() {
        let options = {
            version: VERSION,
            max: -1
        };

        return PDF(dataBuffer, options).then(function(data) {
            assert.equal(data.numpages, PDF_PAGE_COUNT);
        });
    });


    it(`should pass parse with option max:${PDF_PAGE_COUNT-1}`, function() {
        let options_01 = {
            version: VERSION,
            max: PDF_PAGE_COUNT - 1
        };

        let options_02 = {
            version: VERSION
        };

        return PDF(dataBuffer, options_01).then(function(data) {
            assert.equal(data.numpages, PDF_PAGE_COUNT);

        }).then(function() {
            return PDF(dataBuffer, options_02).then(function(data) {
                assert.equal(data.numpages, PDF_PAGE_COUNT);
            });
        });
    });

});
